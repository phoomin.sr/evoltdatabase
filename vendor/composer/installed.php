<?php return array(
    'root' => array(
        'pretty_version' => 'dev-master',
        'version' => 'dev-master',
        'type' => 'library',
        'install_path' => __DIR__ . '/../../',
        'aliases' => array(),
        'reference' => '434fcdcee437731340fdea7cebbe4cd01523b5b1',
        'name' => 'minostra/models',
        'dev' => true,
    ),
    'versions' => array(
        'minostra/models' => array(
            'pretty_version' => 'dev-master',
            'version' => 'dev-master',
            'type' => 'library',
            'install_path' => __DIR__ . '/../../',
            'aliases' => array(),
            'reference' => '434fcdcee437731340fdea7cebbe4cd01523b5b1',
            'dev_requirement' => false,
        ),
    ),
);
